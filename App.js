import React, {Component, useEffect} from 'react';
import {TextInput, View, TouchableOpacity, Text, StyleSheet, Alert} from 'react-native';


class HalamanDepan extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          namaDepan: '',
          namaBelakang: '',
          namaLengkap: '',
        };
      }
    
      ambilNamaDepan = (text) => {
        this.setState({ namaDepan : text});
      };

      ambilNamaBelakang = (text) => {
        this.setState({ namaBelakang : text});
      };

      Hasil = () => {
        let inputSatu = this.state.namaDepan;
        let inputDua = this.state.namaBelakang;
        if (
            inputSatu !== null &&
            inputDua !== null &&
            inputSatu !== " " &&
            inputDua !== " "
        ) {
            this.setState({ namaLengkap: inputSatu + " " + inputDua });
        } else {
            Alert.alert("Nama masih kosong...");
        }
      };


      render () {    
        return (
            <View style={styles.bungkus}>
                <TextInput 
                    style={styles.form}
                    placeholder = "Nama Depan"
                    onChangeText={(text) => this.ambilNamaDepan(text)}
                />
                <TextInput 
                    style={styles.form}
                    placeholder = "Nama Belakang"
                    onChangeText={(text) => this.ambilNamaBelakang(text)}
                />
                <TouchableOpacity style={styles.tombol}
                    onPress={() => this.Hasil()}>
                    <Text style={{
                        color : 'yellow',
                        textAlign : 'center',
                        position : 'absolute'}}>Lihat Hasil</Text>
                </TouchableOpacity>
                <Text style={styles.displayHasil}>
                    {this.state.namaLengkap}
                </Text>
            </View>
        );
    }
}

export default HalamanDepan;

const styles = StyleSheet.create({
    bungkus: {
        flex : 1,
        flexDirection : 'column',
        backgroundColor : 'blue',
        alignItems : 'center',
        justifyContent : 'center',
        position : 'relative',
    },
    form : {
        height : 40,
        width : 'auto',
        backgroundColor : '#ededed',
        textAlign : 'center',
        margin : 3,
        padding : 10,
    },
    tombol : {
        alignItems : 'center',
        backgroundColor : 'green',
        // borderRadius : 100,
        height : 40,
        width : 200,
        alignItems : 'center',
        margin : 25,
        justifyContent : 'center',
        paddingTop : 0,
    },
    text : {
        color : '#0b809c',
        bottom : 10,
        position : 'absolute',
        textAlign : 'center',
    },
    displayHasil: {
        textTransform: "uppercase",
        textAlign: "center",
        fontSize: 20,
        fontWeight: "bold",
        borderBottomColor: "black",
        height: 40,
        width: "100%",
      },
    
});